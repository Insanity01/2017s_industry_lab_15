package ictgradschool.industry.lab15.ex01;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vwen239 on 8/01/2018.
 */
public class NestingShape extends Shape {

    List<Shape> shapes = new ArrayList<>();


    public NestingShape() {

    }

    public NestingShape(int x, int y) {
        super(x, y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);

    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);

    }

    @Override
    public void move(int width, int height) {
        super.move(width, height);

        for (Shape s : shapes) {
            s.move(fWidth, fHeight);
        }

    }

    @Override
    public void paint(Painter painter) {
        painter.drawRect(fX, fY, fWidth, fHeight);
        painter.translate(fX, fY);

        for (Shape s : shapes) {
            s.paint(painter);
        }
    }

    public void add(Shape child) throws IllegalArgumentException {

        if (child.parent != null) {
            throw new IllegalArgumentException("Shape already exists");
        }

        if ((child.fX + child.fWidth) >= this.fWidth || (child.fY + child.fHeight) >= this.fHeight) {
            throw new IllegalArgumentException("Shape doesn't fit");
        }
        shapes.add(child);
        child.parent = this;
    }

    public void remove(Shape child) {

        if (child.parent.equals(this)) {
            child.parent = null;
            shapes.remove(child);
        }

    }


    public Shape shapeAt(int index) throws IndexOutOfBoundsException {

        if (index < 0 || index >= shapes.size()) {
            throw new IndexOutOfBoundsException("Invalid index number");
        } else {
            return shapes.get(index);
        }

    }

    public int shapeCount() {

        return shapes.size();
    }

    public int indexOf(Shape child) {

        return shapes.indexOf(child);

    }

    public boolean contains(Shape child) {

        return shapes.contains(child);
    }


//    Returns the Shape at a specified position within a NestingShape. If the position
//    specified is less than zero or greater than the number of children stored in the
//    NestingShape less one this method throws an IndexOutOfBoundsException.


}
