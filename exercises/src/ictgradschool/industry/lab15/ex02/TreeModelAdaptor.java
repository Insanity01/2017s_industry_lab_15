package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vwen239 on 8/01/2018.
 */
public class TreeModelAdaptor implements TreeModel {

    private NestingShape root;

    List<TreeModelListener> treeModelList = new ArrayList<>();

    public TreeModelAdaptor(NestingShape root) {
        this.root = root;
    }

    public NestingShape getRoot() {
        return root;
    }

    @Override
    public int getChildCount(Object parent) {

        if (parent instanceof NestingShape) {
            return ((NestingShape) parent).shapeCount();
        }

        return 0;
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {

        return ((NestingShape) parent).indexOf((Shape) child);
    }

    @Override
    public Object getChild(Object parent, int index) {
        return ((NestingShape) parent).shapeAt(index);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        treeModelList.remove(l);
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        treeModelList.add(l);
    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof NestingShape);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }
}
